#ifndef PILA_H
#define PILA_H
#include "Constantes.h"


struct _pila;

typedef struct _pila *Pila;

// Crea una Pila.
Pila CrearPila();

//Si es vacia una pila
bool Es_Vacia(Pila pila);


// Inserta una palabra en la pila.
void InsertarTop(Pila &pila,Cadena palabraAIngresar);


// Imprime las ultimas palabras.
void ImprimirPila(Pila pila);


#endif