#include <iostream>
#include <string.h>
#include "Constantes.h"

using namespace std; 


struct _stack
{
	Cadena elem;
	_stack* sig;
};

typedef struct _stack *NodoPila;


struct _pila                     //ESTRUCTURA PALABRAS
    {
        _stack *top;
		unsigned int cantPil; 
    };
typedef struct _pila *Pila;
	

	
// Crea una Pila.
Pila CrearPila()
{
	Pila pila = new _pila;
	pila->top = NULL;
	pila->cantPil = 0;
	return pila;
}


bool Es_Vacia(Pila pila)
{
	return pila->top == NULL;
}

void InsertarTop(Pila &pila,Cadena palabraAIngresar)
{
	if (pila->top == NULL)
	{
		NodoPila aux = new _stack;
		aux->elem = new char;
		strcpy(aux->elem,palabraAIngresar);
		aux->sig = NULL;
		pila->top = aux;
		pila->cantPil++;
	}
	else
	{
		NodoPila aux = new _stack;
		aux->elem = new char;
		strcpy(aux->elem,palabraAIngresar);
		aux->sig = pila->top;
		pila->top = aux;
		pila->cantPil++;
	}
}


void ImprimirPila(Pila pila)
{
	if(pila->top !=NULL)
	{
		NodoPila auxIter = pila->top;
		if (auxIter->sig !=NULL)
		{
			int cont = 2;
			while(auxIter->sig !=NULL && cont<= MAX_CANT_ULTIMAS_PALABRAS)
			{
				cout << auxIter->elem <<endl;
				auxIter = auxIter->sig;
				cont++;
			}
			cout << auxIter->elem <<endl;
		}
		else 
		{
			cout<< auxIter->elem <<endl;
		}
	}
}