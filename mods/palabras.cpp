//palabras.cpp
//

#include <iostream>
#include <string.h>
#include "Constantes.h"
#include "Palabras.h"
#include "Texto.h"
#include <ctype.h>
#include "Diccionario.h"
using namespace std; 

struct nodoPal                      //ESTRUCTURA PALABRAS
    {
        Cadena palabra;
        nodoPal *sig;    
    };

typedef struct nodoPal *NodoPalabra;

struct _palabra                     //SUPER NODO
    {                                               
        unsigned int cantPals;      //Cantidad de nodos de palabras
        nodoPal *primerPal;
        nodoPal *ultimaPal;
        
    }; 


Palabras CrearPalabras()
    {
        Palabras p = new _palabra;
        p->cantPals=0;
        p->primerPal=NULL;
        p->ultimaPal=NULL;
        return p;
    }

TipoRetorno InsertarPalabra(Palabras &palabras, Posicion posicionPalabra, Cadena palabraAIngresar)
{
      
	if (posicionPalabra >= 1 && posicionPalabra <= palabras->cantPals+1)
	{
		int cont = 1;
		NodoPalabra iter = palabras->primerPal;
		NodoPalabra iaux = NULL;
		while (iter!=NULL && posicionPalabra != cont)
		{
			cont++;
			iaux = iter;
			iter = iter->sig;
		}
		NodoPalabra aux = new nodoPal;     
		aux->palabra = new char;
		strcpy(aux->palabra,palabraAIngresar);
		aux->sig = NULL;
		palabras->cantPals++;
		
		if (iter != NULL)
		{
			if (iaux == NULL)
			{
				aux->sig = palabras->primerPal;
				palabras->primerPal = aux;
			}
			else
			{
				aux->sig = iter;
				iaux->sig = aux;
			}
		}
		else
		{
			if (iaux !=NULL)
			{
				iaux->sig = aux;
				palabras->ultimaPal = aux;
			}
			else
			{				
				palabras->primerPal = aux;
				palabras->ultimaPal = aux;
			}
		}
		return OK;
	}
	else 
    {
			return ERROR_2;
    }   
}


TipoRetorno BorrarPalabra(Palabras &palabras, Posicion posicionPalabra)
{
    if (posicionPalabra >= 1 && posicionPalabra <= palabras->cantPals)
	{
		int cont = 1;
		NodoPalabra iter = palabras->primerPal;
		NodoPalabra aux = NULL;
		while (iter!=NULL && posicionPalabra != cont)
		{
			cont++;
			aux = iter;
			iter = iter->sig;
		}
		if (iter != NULL)
		{
			if (aux == NULL)
			{
				palabras->primerPal = iter->sig;
				if(palabras->primerPal == NULL)
				{
					palabras->ultimaPal = NULL;
				}
			}
			else
			{
				aux->sig = iter->sig;
				if(aux->sig == NULL)
				{
					palabras->ultimaPal = aux;
				}						
			}
			delete iter->palabra;
			delete iter;
			palabras->cantPals--;
		}
		return OK;
	}
	else 
	{
		return ERROR_2;
	}
}


void BorrarOcurr(Palabras palabras,NodoPalabra palab, unsigned int cantidadPalabra, Cadena palabraABorrar) {
    if (palab == NULL) {

    } else {
        BorrarOcurr(palabras, palab->sig, cantidadPalabra + 1, palabraABorrar);
        Cadena aBorrar = ConvertirToLow(palabraABorrar);
        Cadena pal = ConvertirToLow(palab->palabra);
        if (strcmp(aBorrar, pal) == 0) {
            BorrarPalabra(palabras, cantidadPalabra);
        }
    }
}

TipoRetorno BorrarOcurrenciasPalabra(Palabras &palabras, Cadena palabraABorrar)
{
	BorrarOcurr(palabras, palabras->primerPal, 1,palabraABorrar);
}

void BorrarTodoPalabras(Palabras &palabras)
{
	
	while (palabras->primerPal !=NULL)
	{
		BorrarPalabra(palabras, 1);
	}
}

void ImprimirPalabras(Palabras palabras)
    {
		NodoPalabra auxIter = palabras->primerPal;

		while (auxIter != NULL)
		{
			cout << auxIter->palabra << " ";
			auxIter = auxIter->sig;
		}
    }

unsigned int CantidadPalabras(Palabras palabras)
    {
        return palabras->cantPals;
    }
    
TipoRetorno PalabraEnPosicion(Palabras palabras, Posicion posicionPalabra,Cadena &palabra)
    {
    	if (posicionPalabra >= 1 && posicionPalabra <= palabras->cantPals && palabras->primerPal != NULL)
    	//hacer Cadena
		palabra = new char; //antes de llamar a esta funcion
		NodoPalabra l = palabras->primerPal;
		NodoPalabra s = NULL;
		int cont = 1;
		while(posicionPalabra != cont)
		{
			s = l;
			l = l->sig;
			cont++;
		}
		strcpy(palabra,l->palabra);
		return OK;
    }

Cadena ConvertirToLow (Cadena palabraAConvertir)
	{
		Cadena aux = new char;
		strcpy(aux,palabraAConvertir);
	
		for( int i=0; i<strlen(aux); i++ )
		{
			aux[i] = tolower(aux[i]);
		}
		return aux;
	}

Cadena UltimaPalabra(Palabras copiar)
	{
		Cadena aux = new char;
		PalabraEnPosicion(copiar, CantidadPalabras(copiar),aux);
		return aux;
	}   
	
Cadena PrimerPalabra(Palabras copiar) 
{ 
  Cadena aux = new char; 
  PalabraEnPosicion(copiar,1,aux); 
  return aux; 
} 
////////////////////////////////DICCIONARIO////////////////////////////////


void ImprimirPalabrasINCO(Diccionario  D, Palabras palabras)
{
    NodoPalabra auxIter = palabras->primerPal;

    while (auxIter != NULL)
    {
        if(PerteneceDiccionario(D, auxIter->palabra))
        {
            auxIter = auxIter->sig;
        }
        else
        {
            cout << auxIter->palabra << " ";
            auxIter = auxIter->sig;
        }
    }
}
