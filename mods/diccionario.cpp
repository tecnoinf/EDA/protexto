#include "Diccionario.h"
#include "Palabras.h"
#include  <stdio.h>
#include  <string.h>
#include <iostream>
using namespace std;


struct _diccionario
{
 Cadena dato;
 _diccionario* der;
 _diccionario* izq;
};

//typedef struct _diccionario* Diccionario ;


Diccionario CrearDiccionario()
{
    return NULL;
}

bool EsVacio(Diccionario D)
{
	return D==NULL;
}


TipoRetorno IngresarPalabraDiccionario(Diccionario &D, Cadena palabraAIngresar)
// Agrega una palabra al diccionario. Esta operación debe realizarse en a lo
// sumo O(log2 n) promedio.
// Ingresa una palabra al diccionario si ésta no se encuentra en el mismo.

{
if (D==NULL)
    {
     D = new _diccionario;
     D->dato = palabraAIngresar;
     D->der = NULL;
     D->izq =NULL;
    return OK;
    }

else if(strcmp(ConvertirToLow(palabraAIngresar), ConvertirToLow(D->dato))>0)  //Retorna >0 cuando string1 > string 2 Retorna <0 cuando string1 < strin2
    {
         return IngresarPalabraDiccionario(D->der, palabraAIngresar);

    }

else if(strcmp(ConvertirToLow(palabraAIngresar), ConvertirToLow(D->dato))<0)
    {
        return IngresarPalabraDiccionario(D->izq, palabraAIngresar);

    }

}



Cadena borrarMin(Diccionario &D)
{
    if(D->izq==NULL)
    {
        Diccionario temporal=D;
        D=D->der;
        Cadena res=temporal->dato;
        delete temporal;
        return res;
    }
    else
        borrarMin(D->izq);
}

TipoRetorno BorrarPalabraDiccionario(Diccionario &D, Cadena palabraABorrar)
// Borra una palabra del diccionario si se encuentra en el mismo.

{
    if(D!=NULL)
    {
            if(strcmp(ConvertirToLow(D->dato), ConvertirToLow(palabraABorrar))==0)
            {
                Diccionario temporal=D;
                if(D->izq==NULL)
                {
                 D=D->der;
                 delete temporal;
                }
            else if(D->der==NULL)
                {
                 D=D->izq;
                 delete temporal;
                }
            else
                {
                    Cadena min =borrarMin(D->der);
                    D->dato=min;
                }
            }
    else if(strcmp(ConvertirToLow(palabraABorrar), ConvertirToLow(D->dato))<0)
                BorrarPalabraDiccionario(D->izq, palabraABorrar);
        else
                BorrarPalabraDiccionario(D->der, palabraABorrar);


    }
}



bool PerteneceDiccionario(Diccionario D, Cadena palabra)
// Devuelve true si y sólo si la palabra pertenece al diccionario
{
	if (D == NULL)
	{
		return false;
	}
	else if(strcmp(ConvertirToLow(palabra), ConvertirToLow(D->dato))==0)
	{
		return true;
	}
	else if (strcmp(ConvertirToLow(palabra), ConvertirToLow(D->dato))<0)
	{
		return PerteneceDiccionario(D->izq,palabra);
	}
	else
	{
		return PerteneceDiccionario(D->der,palabra);
	}
}

//verficar afuera si es null e imprimir "diccionario vacio"
TipoRetorno ImprimirDiccionario(Diccionario D)
{
	if (D != NULL)
	{
		ImprimirDiccionario(D->izq);
		cout<<D->dato<<endl;
		ImprimirDiccionario(D->der);
		return OK;
	}
}

// Muestra las palabras del diccionario odenadas alfabéticamente, de menor a
// mayor.
// Esta operación debe realizarse en O(n) peor caso.
// Cuando el diccionario no tiene palabras debe mostrarse el mensaje
// "Diccionario vacio".





