#ifndef CONSTANTES_H
#define CONSTANTES_H

#define MAX_CANT_PALABRAS_X_LINEA 20
#define MAX_LARGO_PALABRA 15
#define MAX_CANT_ULTIMAS_PALABRAS 3

typedef char *Cadena;

enum _retorno { OK, ERROR,ERROR_2, NO_IMPLEMENTADA };

typedef enum _retorno TipoRetorno;

typedef unsigned int Posicion;

#endif
