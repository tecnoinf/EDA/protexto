//Texto.cpp
//
#include <iostream>
#include "Constantes.h"
#include "Palabras.h"
#include "Texto.h"
#include "Diccionario.h"

using namespace std; 			//cout, cin (se usa para no escribir std::___)

//Estructuras:
struct _linea                       //Estructura Linea
	{
        Palabras cabeza;            //Puntero al primer nodo de la linea
        _linea *sig;                //Sig Puntero a nueva linea
    };

typedef struct _linea *lineas;


struct _texto                       //Estructura Unica TEXTO
    {
        unsigned int cantLineas;   //Entero cantidad de lineas en el texto
        lineas linea;              //Puntero linea a la proxima lnea

    };


//Funciones:

Texto CrearTexto()
    {
        Texto texto = new _texto;
        texto->cantLineas = 0;
        texto->linea = NULL;
        return texto;
    }


TipoRetorno InsertarLinea(Texto &texto)
	{
		lineas aux = new _linea;
		aux->cabeza = CrearPalabras();
		aux->sig = texto->linea;
		texto->linea = aux;
        texto->cantLineas++;
        return OK;
    }
      


TipoRetorno InsertarLineaEnPosicion(Texto &texto, Posicion posicionLinea)
{
		//controlar en comando -> if (posicionLinea >= 1 && posicionLinea <= texto->cantLineas + 1)
	if (posicionLinea == texto->cantLineas + 1)
	{
		InsertarLinea(texto);
		return OK;
	}	
	else if (posicionLinea == 1) 
	{
		lineas l = texto->linea;
		lineas s = NULL;
		while(l != NULL && l->sig != NULL)
		{
			s = l;
			l = l->sig;		
		}
		lineas aux = new _linea;
		aux->cabeza = CrearPalabras();
		aux->sig = NULL;
		l->sig = aux;
		texto->cantLineas++;
		return OK;
	}
	else
	{
		lineas l = texto->linea;
		lineas s = NULL;
		int cont = texto->cantLineas;
		
		while(posicionLinea != cont)
		{
			s = l;
			l = l->sig;
			cont--;
		}
		lineas aux = new _linea;
		aux->cabeza = CrearPalabras();
		aux->sig = l->sig;
		l->sig = aux;
		texto->cantLineas++;
		return OK;
	}
}

TipoRetorno BorrarLinea(Texto &texto, Posicion posicionLinea)
{
        //controlar en comandos-> if (posicionLinea >= 1 && posicionLinea <= texto->cantLineas)
	if (texto->cantLineas == 0)
	{
		return OK;
	}
	else
	{
		if (posicionLinea == texto->cantLineas)
		{
			lineas tmp = texto->linea;
			texto->linea = texto->linea->sig;
			BorrarTodoPalabras(tmp->cabeza);
			delete tmp;
			texto->cantLineas--;
			return OK;
		}	
		else if (posicionLinea == 1) 
		{
			lineas l = texto->linea;
			lineas s = NULL;
			while(l != NULL && l->sig != NULL)
			{
				s = l;
				l = l->sig;		
			}
			lineas tmp = l;
			l = s;
			BorrarTodoPalabras(tmp->cabeza);
			delete tmp;
			texto->cantLineas--;
			return OK;
		}
		else
		{
			lineas l = texto->linea;
			lineas s = NULL;
			int cont = texto->cantLineas;
			while(posicionLinea != cont)
			{
				s = l;
				l = l->sig;
				cont--;
			}
			lineas tmp = l;
			l = l->sig;
			s->sig = l;
			BorrarTodoPalabras(tmp->cabeza);
			delete tmp;
			texto->cantLineas--;
			return OK;
		}
	}
}

TipoRetorno BorrarTodo(Texto &texto)
{
    if (texto->linea == NULL)
	{
		return OK;
	}
	else
	{
		BorrarLinea(texto,texto->cantLineas);
		return BorrarTodo(texto);	
	}
}



TipoRetorno BorrarOcurrenciasPalabraEnTexto(Texto texto, Cadena palabraABorrar)
{
	lineas aux = texto->linea;

	while (aux != NULL)
	{
		BorrarOcurrenciasPalabra(aux->cabeza, palabraABorrar);
		aux = aux->sig;
	}
	return OK;
}

void imprimir(lineas l, unsigned int &cantidadLineas)
{
	if (l != NULL)
	{
		imprimir(l->sig, --cantidadLineas);
		cantidadLineas++;
		cout<< cantidadLineas <<": ";
		ImprimirPalabras(l->cabeza);
		cout << endl;
	}
}

TipoRetorno ImprimirTexto(Texto texto)  
{	
	if (texto->linea == NULL)
	{
		return OK;
	}
	else
	{
		imprimir(texto->linea, texto->cantLineas);
		return OK;
	}
}
	


void NPI(Texto texto, lineas linea, unsigned int &cantLineas)
{
	if (linea != NULL && linea->sig != NULL)
	{
		NPI(texto, linea->sig, --cantLineas);
		++cantLineas;
		while (CantidadPalabras(linea->cabeza)>0 && CantidadPalabras(linea->sig->cabeza)<MAX_CANT_PALABRAS_X_LINEA)
		{
			InsertarPalabra(linea->sig->cabeza, CantidadPalabras(linea->sig->cabeza)+1, PrimerPalabra(linea->cabeza));
			BorrarPalabra(linea->cabeza, 1);
		}
		if (CantidadPalabras(linea->cabeza)==0)
		{
			BorrarLinea(texto,cantLineas);
			cantLineas--;
		}
	}
}

TipoRetorno ComprimirTexto(Texto texto)
{

    if (texto->linea == NULL)
    {
     	return OK;
    }

	else
	{
        unsigned int aux = texto->cantLineas;
		NPI(texto, texto->linea, aux);
		return OK;
	}

}


TipoRetorno Insert(Texto &texto,lineas linea,unsigned int cont, Posicion posicionLinea, Posicion posicionPalabra, Cadena palabraAIngresar)
{
    if(cont == posicionLinea)
	{
        return InsertarPalabra(linea->cabeza, posicionPalabra, palabraAIngresar);
    }
	else
	{
        TipoRetorno res_err = Insert(texto,linea->sig,cont - 1, posicionLinea, posicionPalabra, palabraAIngresar);
        if(linea->sig != NULL && CantidadPalabras(linea->sig->cabeza) > MAX_CANT_PALABRAS_X_LINEA )
		{
            Cadena palACorrer = new char;
            PalabraEnPosicion(linea->sig->cabeza, MAX_CANT_PALABRAS_X_LINEA+1, palACorrer);
            BorrarPalabra(linea->sig->cabeza, MAX_CANT_PALABRAS_X_LINEA+1);
            InsertarPalabra(linea->cabeza, 1, palACorrer);
        }
		return res_err;
    }
}

TipoRetorno InsertarPalabraEnLinea(Texto texto, Posicion posicionLinea, Posicion posicionPalabra, Cadena palabraAIngresar)
{   
	TipoRetorno res = Insert(texto,texto->linea,texto->cantLineas,posicionLinea,posicionPalabra,palabraAIngresar);

	if(CantidadPalabras(texto->linea->cabeza) > MAX_CANT_PALABRAS_X_LINEA)
	{
		InsertarLinea(texto);
		Cadena palACorrer = new char;
		PalabraEnPosicion(texto->linea->sig->cabeza, MAX_CANT_PALABRAS_X_LINEA+1, palACorrer);
		BorrarPalabra(texto->linea->sig->cabeza, MAX_CANT_PALABRAS_X_LINEA+1);
		InsertarPalabra(texto->linea->cabeza, 1, palACorrer);
	}
	return res;

}



TipoRetorno BorrarPalabraEnLinea(Texto texto, Posicion posicionLinea,Posicion posicionPalabra)
    {
		lineas l = texto->linea; //para recorrer creo un puntero al primer elemento de la lista (ultima linea del texto)
		lineas s = NULL; //para recorrer creo un puntero a NULL que va a ir un elemento "atras" de l
		int cont=texto->cantLineas; 
		while (cont > posicionLinea) // mientras el contador sea mayor a la posicionLinea
		{
			s = l; 
			l=l->sig;
			cont--;    
		}        
		return BorrarPalabra(l->cabeza, posicionPalabra);
	}

TipoRetorno BorrarOcurrenciasPalabraEnLinea(Texto texto, Posicion posicionLinea,Cadena palabraABorrar)
    {				
			lineas l = texto->linea; //para recorrer creo un puntero al primer elemento de la lista (ultima linea del texto)
			int cont=texto->cantLineas; 
			while (cont > posicionLinea) // mientras el contador sea mayor a la posicionLinea
			{
				l=l->sig;
				cont--;    
			}
			return BorrarOcurrenciasPalabra(l->cabeza,palabraABorrar);			   			  
	}

TipoRetorno ImprimirLinea(Texto texto, Posicion posicionLinea)
{
		//if (posicionLinea >=1 && posicionLinea <= texto->cantLineas)
    	//{	

			
    		lineas l = texto->linea; //para recorrer creo un puntero al primer elemento de la lista (ultima linea del texto)
			int cont=texto->cantLineas; 
		
    		while (cont > posicionLinea) // mientras el contador sea mayor a la posicionLinea
				{
					l=l->sig;
					cont--;    
				}
			cout<<cont<<": ";	
			ImprimirPalabras(l->cabeza);
    		
    		return OK;
}

int CantidadLineas(Texto texto)
{
	return texto->cantLineas;
}
//////////////////////////////////////////DICCIONARIO/////////////////////////////////

void imprimirTextoSD(Diccionario D, lineas l, unsigned int &cantidadLineas)
{
    if (l != NULL)
    {
        imprimirTextoSD(D, l->sig, --cantidadLineas);
        cantidadLineas++;
        cout<< cantidadLineas <<": ";

        ImprimirPalabrasINCO(D,l->cabeza);
        cout << endl;
    }
}

TipoRetorno ImprimirTextoSinDiccionario(Texto texto, Diccionario D)
{
    if (texto->linea == NULL)
    {
        return OK;
    }
    else
    {
        imprimirTextoSD(D, texto->linea, texto->cantLineas);
        return OK;
    }
}





