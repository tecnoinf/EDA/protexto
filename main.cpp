//Main 
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "mods/Constantes.h"
#include "mods/Palabras.h"
#include "mods/Diccionario.h"
#include "mods/Pila.h"
#include "mods/Texto.h"
#include <string.h>
using namespace std; 
#define clearscr() printf("\033[H\033[J");

//Texto texto;




int main()
{ 
    Texto textoto = CrearTexto();
	Diccionario D = CrearDiccionario();
	Pila pila = CrearPila();
    
    int errorOK;
    Posicion posicionPalabra = 0;      
    Posicion posicionLinea = 0;  
    Cadena palabraABorrar; 
	Cadena palabraIngresar;
	Cadena palabra;
    int opcion=0;
    bool optsal=false;
 do
 {
    clearscr();
    cout<<"     1 - Insertar nueva linea vacia. " << endl;
    cout<<"     2 - Insertar linea en posicion especifica. "<< endl;
    cout<<"     3 - Borrar linea en posicion especifica. " << endl;
    cout<<"     4 - Borrar todas las lineas del texto.  " << endl;
    cout<<"     5 - Borrar ocurrencias de palabras en el texto. " <<endl;
    cout<<"     6 - Imprime el texto por pantalla. " <<endl;
    cout<<"     7 - Comprime las palabras del texto. "<<endl;
    cout<<"     8 - Inserta una palabra en una linea. "<<endl;
    cout<<"     9 - Borra la palabra en la posicion indicada. " <<endl;
    cout<<"    10 - Borra todas las ocurrencias de una palabra en la linea indicada. "<<endl;
    cout<<"    11 - Imprime la linea por pantalla. " <<endl;
	cout<<"    12 - Agregar palabra al diccionario. "<<endl;
    cout<<"    13 - Borrar palabra del diccionario. "<<endl;
    cout<<"    14 - Imprimir el diccionario. " <<endl;
    cout<<"    15 - Imprimir el texto con las palabras que no estan en el diccionario. "<<endl;
    cout<<"    16 - Imprimir las ultimas palabras ingresadas al texto. " <<endl;
    cout<<"    17 - Para SALIR"<<endl;
    cout<< string(3, '\n');
  	cout<<"	Selecione una opcion [1-17]: ";
  	cin>> opcion;
    cout<< opcion << endl; 
    
switch (opcion) 
{
	case 1:
	{
        clearscr();            
  	    cout<<"----------------------------OPCION 1--------------------------------- \n"<<endl;
		InsertarLinea(textoto);
        cout<< "La linea se inserto correctamente." << endl;
		cout<<"Cantidad de lineas: "<<CantidadLineas(textoto)<<endl;
		cin.get();
	}
    break;
	    
  	case 2:
	{
        bool listo = false;
  	    clearscr();
        cout<<"----------------------------OPCION 2--------------------------------- \n" <<endl;
        cout<<endl;
        
        do
		{
			cout<<"Digite la posicion en la que desee insertar la linea:  ";
			cin>>posicionLinea;
			cout<<endl;
			if (posicionLinea < 1 || posicionLinea > CantidadLineas(textoto) + 1)
			{
				cout<<"Digite una posicion valida."<<endl; 
			}
            else
			{
				listo = true;
			}
        }while (! listo);
			
           cout<<"Cantidad de lineas ANTES: "<<CantidadLineas(textoto)<<endl;
           InsertarLineaEnPosicion(textoto, posicionLinea);
           cout<<"Cantidad de lineas AHORA: "<<CantidadLineas(textoto)<<endl;
           cout<< "La linea se inserto correctamente." << endl;
           cin.get();
	}      
	break;
	   
    case 3:
	{
		clearscr();    
		bool listo = false;
		cout<<"----------------------------OPCION 3--------------------------------- \n"<<endl;
		cout<<endl;
		do
		{
			cout<<"Digite la posicion de la linea que desea borrar:  ";
			cin>>posicionLinea;
			cout<<endl;
			if (posicionLinea < 1 || posicionLinea > CantidadLineas(textoto))
			{
				if (CantidadLineas(textoto) == 0)
				{
					cout<<"No hay lineas para borrar"<<endl; 
					listo = true;
				}
				else
				{
				cout<<"Digite una posicion valida."<<endl; 
				}
			}
            else
			{
				listo = true;
			}
        }while (! listo);
		
        cout<<"Cantidad de lineas ANTES: "<<CantidadLineas(textoto)<<endl;
        BorrarLinea(textoto, posicionLinea);
        cout<<"Cantidad de lineas AHORA: "<<CantidadLineas(textoto)<<endl;
		cin.ignore().get(); 
	}     
	break;
     
  	case 4:
	{
		clearscr();            
    	cout<<"----------------------------OPCION 4--------------------------------- \n"<<endl;
		char opt;
		cout<<endl;
        cout<<"¿Esta seguro que desea borrar todas las lineas?"<<endl;
		cout<<"Digite (S) para continuar:   ";
		cin>>opt;
		if (opt == 'S' || opt == 's')
		{
			BorrarTodo(textoto);
			cout<<"Se borraron todas las lineas"<<endl;
			cout<<endl;
			cin.get(); 
		}
		cin.get(); 
    }
    break;
    
    case 5:  
	{
		palabraABorrar = new char;
  	    clearscr();
		cout<<"----------------------------OPCION 5--------------------------------- \n"<<endl;
		cout<<endl;
		cout<<" Ingrese la palabra que desee eliminar en este documento y presione Enter: ";
		cin>>palabraABorrar;
		cout<<endl;
        BorrarOcurrenciasPalabraEnTexto(textoto,palabraABorrar);
		cout<<" Se borro ocurrencias de la palabra "<<palabraABorrar<<" en el texto"<<endl;;
		cout<<endl;
        cin.ignore().get(); 
    }
    break;

    case 6:
	{
		clearscr();
        cout<<"----------------------------OPCION 6--------------------------------- \n"<<endl;
		cout<<endl;
		if (CantidadLineas(textoto) == 0)
		{
			cout<<"Texto vacio."<<endl;
			cout<<"Digite enter para continuar..."<<endl; 
			cin.ignore().get(); 
		}
		else
		{
			ImprimirTexto(textoto);
			cin.ignore().get(); 
		}
	}
    break;    
       
    case 7:
    {
        clearscr();
        cout<<"----------------------------OPCION 7--------------------------------- \n"<<endl;
		cout<<endl;
		if (CantidadLineas(textoto) == 0)
		{
			cout<<"El texto es vacio."<<endl;
			cin.ignore().get(); 
		}
		else
		{
			ComprimirTexto(textoto);
			cout<<"Se comprimio el texto correctamente."<<endl;
			cin.ignore().get();
		}
		
	}
    break; 
      
    case 8:
    {   
		Cadena palabraIngresar = new char;
		Cadena palabraPila = new char;
		TipoRetorno error;
		bool listo = false;
		clearscr();
		cout<<"----------------------------OPCION 8--------------------------------- \n"<<endl;   
		cout<<endl;
		do
		{
			cout<<"Digite la posicion de la linea:  ";
			cin>>posicionLinea;
			cout<<endl;
			cout<<" Ingrese la posicion donde desea insertar la nueva palabra, luego presione enter:  ";
			cin>>posicionPalabra; 
			cout<<endl;
			cout<<" Ingrese la palabra que desea insertar, luego presione enter:  ";
			cin>>palabraIngresar;
			cout<<endl;
			
			if (posicionLinea < 1 || posicionLinea > CantidadLineas(textoto))
			{
				if (CantidadLineas(textoto) == 0)
				{
					cout<<"Antes de insertar una palabra debe insertar una linea con la OPCION 1"<<endl; 
					cout<<endl;
					listo = true;
				}
				else
				{
				cout<<"Digite una posicion de linea valida."<<endl;
				cout<<endl;
				}
			}
			else
			{
				strcpy(palabraPila,palabraIngresar);
				error = InsertarPalabraEnLinea(textoto,posicionLinea, posicionPalabra, palabraIngresar);
				if (error == ERROR_2)
				{
					cout<<"Digite una posicion de palabra valida."<<endl; 
					cout<<endl;
				}
				else
				{
					cout<<"Se ingreso la palabra correctamente."<<endl; 
					InsertarTop(pila,palabraIngresar);
					listo = true;
				}
			}
			
		}while(! listo);
		cin.ignore().get(); 
	}
	break;  
      
   case 9:
   {
        clearscr();
		bool listo = false;
		TipoRetorno error;
        cout<<"----------------------------OPCION 9--------------------------------- \n"<<endl;   
		cout<<endl;
		do
		{
			cout<<"Digite la posicion de la linea:  ";
			cin>>posicionLinea;
			cout<<endl;
			cout<<" Ingrese la posicion donde desea eliminar la palabra:  ";
			cin>>posicionPalabra; 
			cout<<endl;
			
			if (posicionLinea < 1 || posicionLinea > CantidadLineas(textoto))
			{
				cout<<"Digite una posicion de linea valida."<<endl;
				cout<<endl;
			}
			else
			{
				error = BorrarPalabraEnLinea(textoto, posicionLinea,posicionPalabra);
				if (error == ERROR_2)
				{
					cout<<"Digite una posicion de palabra valida."<<endl; 
					cout<<endl;
				}
				else
				{
					cout<<"Se elimino la palabra correctamente."<<endl; 
					listo = true;
				}
			}
		
		
	}while(! listo);
	cin.ignore().get();
   }
   break;    
     
   case 10:
   {   
		clearscr();
       	bool listo = false;
        cout<<"----------------------------OPCION 10--------------------------------- \n"<<endl;   
		cout<<endl;
		do
		{
			cout<<"Digite la posicion de la linea:  ";
			cin>>posicionLinea;
			cout<<endl;
			cout<<" Ingrese la palabra a borrar:  ";
			cin>>palabraABorrar; 
			cout<<endl;
			
			if (posicionLinea < 1 || posicionLinea > CantidadLineas(textoto))
			{
				cout<<"Digite una posicion de linea valida."<<endl;
				cout<<endl;
			}
			else
			{
			    BorrarOcurrenciasPalabraEnLinea(textoto,posicionLinea,palabraABorrar);
				cout<<"Se elimino la palabra correctamente."<<endl; 
				listo = true;
			
			}
			
		}while(! listo);
		cin.ignore().get();
   }
   break;    
       
   case 11:
   {   
        clearscr();
		bool listo = false;
        TipoRetorno error;
        cout<<"----------------------------OPCION 11--------------------------------- \n"<<endl;   
		cout<<endl;
		if (CantidadLineas(textoto) == 0)
		{
			cout<<"Texto vacio."<<endl;
			cout<<"Digite enter para continuar..."<<endl; 
			cin.ignore().get(); 
		}
		else
		{
			do
			{
			
				cout<<"Digite la linea que desee imprimir:  ";
				cin>>posicionLinea;
				cout<<endl;
				if (posicionLinea < 1 || posicionLinea > CantidadLineas(textoto) + 1)
				{
					cout<<"Digite una posicion valida."<<endl; 
				}
				else
				{
					error = ImprimirLinea(textoto,posicionLinea);
					listo = true;
				}
			}while (! listo);	
			cin.ignore().get();
		}
    }
   break;    
   
    case 12:
   {
        clearscr();
		bool listo = false;
        Cadena palabra = new char;
        cout<<"----------------------------OPCION 12--------------------------------- \n"<<endl;
		cout<<endl;
		do
		{
		
			cout<<"Digite la palabra que desea agregar:  ";
			cin>>palabra;
			cout<<endl;
			if (PerteneceDiccionario(D,palabra))
			{
				cout<<"La palabra ya existe en el diccionario."<<endl; 
			}
            else
			{
				IngresarPalabraDiccionario(D,palabra);
				cout<<"La palabra se agrego al diccionario."<<endl; 
				listo = true;
			}
        }while (! listo);	
		cin.ignore().get();
    }
   break;
   
    case 13:
   {   
        clearscr();
	   	bool listo = false;
	    Cadena palabra = new char;
        cout<<"----------------------------OPCION 13--------------------------------- \n"<<endl;
		cout<<endl;
		do
		{
		
			cout<<"Digite la palabra que desee eliminar:  ";
			cin>>palabra;
			if (!PerteneceDiccionario(D,palabra))
			{
				cout<<"La palabra no existe."<<endl;
				listo = true;
			}
            else
			{
				BorrarPalabraDiccionario(D,palabra);
				cout<<"La palabra se borro correctamente."<<endl; 
				listo = true;
			}
        }while (! listo);	
		cin.ignore().get();
    }
   break;
   
    case 14:
   {   
        clearscr();
        cout<<"----------------------------OPCION 14--------------------------------- \n"<<endl;
		cout<<endl;
		if (EsVacio(D))
		{
			cout<<"Diccionario vacio."<<endl;
			cout<<"Digite enter para continuar..."<<endl; 
			cin.ignore().get(); 
		}
		else
		{
			ImprimirDiccionario(D);
			cout<<endl;
			cout<<"Digite enter para continuar..."<<endl; 
			cin.ignore().get(); 
		}
    }
   break;
   
    case 15:
   {   
        clearscr();
		cout<<"----------------------------OPCION 15--------------------------------- \n"<<endl;
		cout<<endl;
	   if (CantidadLineas(textoto) == 0)
	   {
		   cout<<"Texto vacio."<<endl;
		   cout<<"Digite enter para continuar..."<<endl; 
		   cin.ignore().get();
	   }
	   else
	   {
		   ImprimirTextoSinDiccionario(textoto, D);
		   cout<<endl;
		   cout<<"Digite enter para continuar..."<<endl; 
		   cin.ignore().get();
	   }


	}
   break;
   
    case 16:
   {   
        clearscr();
        cout<<"----------------------------OPCION 16--------------------------------- \n"<<endl;
		cout<<endl;
		if (Es_Vacia(pila))
		{
			cout<<"No se han ingresado palabras"<<endl;
			cout<<"Digite enter para continuar..."<<endl; 
			cin.ignore().get(); 
		}
		else
		{
			ImprimirPila(pila);
			cout<<endl;
			cout<<"Digite enter para continuar..."<<endl; 
			cin.ignore().get(); 
		}
    }
   break;
   
   case 17:
   {     
        clearscr();
        cout<<"----------------------------OPCION 12--------------------------------- \n"<<endl;
        opcion=0;
        clearscr();
        cout<< string(15, '\n');
        cout<<"¿Esta seguro que desa SALIR del programa?";
        cout<<"\n";
        cout<<"     1 - NO \n";
        cout<<"     2 - SI \n";
        cout<<"\n";
        cout<<"     Selecione una opcion [1-2]: ";
        cin>> opcion;
        switch (opcion)
        { 
            case 1:
			{
				cout<<"Retornar al Menú";
			}
            break;
			  
            case 2:
			{
               optsal = true;
				cout<<"HASTA LUEGO"<<endl;

			}
              
		}
    }
}
} while (!optsal);

	cin.ignore().get();
	return 0;
}
